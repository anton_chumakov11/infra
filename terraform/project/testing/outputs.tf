output "test_server_ip" {
  value = module.test.server_ip
}

output "runner_server_ip" {
  value = module.runner.server_ip
}

output "monitoring_server_ip" {
  value = module.monitoring.server_ip
}



output "zone" {
  value = module.dns_zone.zone_id
}
