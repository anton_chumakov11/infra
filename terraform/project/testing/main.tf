terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"


 backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "dagobah88-project-bucket"
    region     = "ru-central1"
    key        = "terraform.tfstate"
    access_key = "YCAJEwoGZTNvKkrZGqBqMniHM"
    secret_key = "YCMgZzrC2Ag7EZQJaeVePCB7sNvI9KMGOA57zuZh"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}



provider "yandex" {
  token     = "AQAAAAAAavf7AATuweN3Juj4zUvwu6jIAF66Zbo"
  cloud_id  = "b1genblbl6n8ukns8o45"
  folder_id = "b1gdeeatjdo5ocpcm2uh"
  zone      = "ru-central1-a"
}


module "test" {
  source = "../../modules/server"
} 


module "monitoring" {
  source = "../../modules/server"
  server_name = "monitorig"
}

module "runner" {
  source = "../../modules/server"
  server_name = "runner"
}


module "ballancer" {
  source = "../../modules/loadbalancer"
  ip_address = module.test.local_ip
}

module "dns_zone" {
  source = "../../modules/dns-zone"
}

module "test-recordset" {
  source = "../../modules/dns-recordset"
  zone_id = module.dns_zone.zone_id
  name = "test.dagobah88.store."
  data = module.ballancer.ip_address
}

module "monitoring-recordset" {
  source = "../../modules/dns-recordset"
  zone_id = module.dns_zone.zone_id
  name = "monitoring.dagobah88.store."
  data = module.monitoring.server_ip
}


module "grafana-recordset" {
  source = "../../modules/dns-recordset"
  zone_id = module.dns_zone.zone_id
  name = "grafana.dagobah88.store."
  data = module.monitoring.server_ip
}

module "runner-recordset" {
  source = "../../modules/dns-recordset"
  zone_id = module.dns_zone.zone_id
  name = "runner.dagobah88.store."
  data = module.runner.server_ip
}

module "test-server-recordset" {
  source = "../../modules/dns-recordset"
  zone_id = module.dns_zone.zone_id
  name = "test-server.dagobah88.store."
  data = module.test.server_ip
}
