terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

}

data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "dagobah88-project-bucket"
    region     = "ru-central1"
    key        = "terraform.tfstate"
    access_key = "YCAJEwoGZTNvKkrZGqBqMniHM"
    secret_key = "YCMgZzrC2Ag7EZQJaeVePCB7sNvI9KMGOA57zuZh"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  token     = "AQAAAAAAavf7AATuweN3Juj4zUvwu6jIAF66Zbo"
  cloud_id  = "b1genblbl6n8ukns8o45"
  folder_id = "b1gdeeatjdo5ocpcm2uh"
  zone      = "ru-central1-a"
}

module "prod" {
  source = "../../modules/server"
  server_name = "prod"
}

module "ballancer" {
  source = "../../modules/loadbalancer"
  ip_address = module.prod.local_ip
  env = "prod"
}

data "yandex_dns_zone" "zone" {
  name = "my-public-zone"
}

module "prod-recordset" {
  source = "../../modules/dns-recordset"
  zone_id = data.terraform_remote_state.vpc.outputs.zone
  name = "dagobah88.store."
  data = module.ballancer.ip_address
}


module "prod-server-recordset" {
  source = "../../modules/dns-recordset"
  zone_id = data.terraform_remote_state.vpc.outputs.zone
  name = "prod-server.dagobah88.store."
  data = module.prod.server_ip
}
