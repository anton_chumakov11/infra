terraform {
  required_providers {
    yandex = {
      source = "terraform-registry.storage.yandexcloud.net/yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

 backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "dagobah88-bucket"
    region     = "ru-central1"
    key        = "terraform.tfstate"
    access_key = "YCAJEwoGZTNvKkrZGqBqMniHM"
    secret_key = "YCMgZzrC2Ag7EZQJaeVePCB7sNvI9KMGOA57zuZh"

    skip_region_validation      = true
    skip_credentials_validation = true
  }


}

provider "yandex" {
  token     = "AQAAAAAAavf7AATuweN3Juj4zUvwu6jIAF66Zbo"
  cloud_id  = "b1genblbl6n8ukns8o45"
  folder_id = "b1gdeeatjdo5ocpcm2uh"
  zone      = "ru-central1-a"
}


data "yandex_compute_image" "my_image" {
  family = "ubuntu-1804-lts"
}


resource "yandex_compute_instance" "server-1" {

  name        = "server"
  platform_id = "standard-v1"

  resources {
    cores = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my_image.id}"
      size = 50
  }
  }
  network_interface {
#    subnet_id = "${yandex_vpc_subnet.subnet-a.id}"
    subnet_id = "e9bt8no9uj177oon79kh"
   nat       = true
  }

    metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }



}

resource "yandex_compute_instance" "gitlab-runner-server" {

  name        = "runner-server"
  platform_id = "standard-v1"

  resources {
    cores = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my_image.id}"
      size = 50
  }
  }
  network_interface {
#    subnet_id = "${yandex_vpc_subnet.subnet-a.id}"
    subnet_id = "e9bt8no9uj177oon79kh"
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }


}


resource "yandex_compute_instance" "prometheus-monitoring-server" {

  name        = "moniroring-server"
  platform_id = "standard-v1"

  resources {
    cores = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my_image.id}"
      size = 50
  }
  }
  network_interface {
#    subnet_id = "${yandex_vpc_subnet.subnet-a.id}"
    subnet_id = "e9bt8no9uj177oon79kh"
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }


}

resource "yandex_alb_target_group" "foo" {
  name      = "my-target-group"

  target {
#    subnet_id = "${yandex_vpc_subnet.subnet-a.id}"
    subnet_id = "e9bt8no9uj177oon79kh"
    ip_address   = "${yandex_compute_instance.server-1.network_interface.0.ip_address}"
  }
}



resource "yandex_alb_virtual_host" "virtual-host" {
  name      = "virtual-host"
  http_router_id = yandex_alb_http_router.my-router.id
  route {
    name = "route"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.backend-group.id
        timeout = "3s"
      }
    }
  }
}



resource "yandex_alb_backend_group" "backend-group" {
  name      = "my-backend-group"

  http_backend {
    name = "backend-1"
    weight = 1
    port = 8080
    target_group_ids = ["${yandex_alb_target_group.foo.id}"]
#    tls {
#      sni = "backend-domain.internal"
#    }
#    load_balancing_config {
#      panic_threshold = 50
#    }    
    healthcheck {
      timeout = "1s"
      interval = "3s"
      http_healthcheck {
        path  = "/"
      }
    }
#    http2 = "true"
  }
}


#resource "yandex_vpc_network" "my-net" {
#  name = "my_net"
#}

#resource "yandex_vpc_subnet" "subnet-a" {
#  v4_cidr_blocks = ["192.168.0.0/16"]
#  zone           = "ru-central1-a"
#  network_id     = "${yandex_vpc_network.my-net.id}"
#}

resource "yandex_alb_http_router" "my-router" {
  name      = "my-http-router"
}


resource "yandex_dns_zone" "zone1" {
  name        = "my-public-zone"
  description = "dagobah88 public zone"

  labels = {
    label1 = "dagobah88-public"
  }

  zone    = "dagobah88.store."
  public  = true
}



resource "yandex_alb_load_balancer" "test-balancer" {
  name        = "dagobah-load-balancer"

#  network_id  = "${yandex_vpc_network.my-net.id}"
  network_id = "enptveg5tus0sr6f6bq1"

  allocation_policy {
    location {
      zone_id   = "ru-central1-a"
#      subnet_id = "${yandex_vpc_subnet.subnet-a.id}" 
      subnet_id = "e9bt8no9uj177oon79kh"
    }
  }

  listener {
    name = "my-listener"
    endpoint {
      address {
        external_ipv4_address {
        }
      }
      ports = [ 80 ]
    }    
    http {
      handler {
        http_router_id = "${yandex_alb_http_router.my-router.id}"
      }
    }
    }
     
}

resource "yandex_dns_recordset" "rs1" {
  zone_id = "${yandex_dns_zone.zone1.id}"
  name    = "test.dagobah88.store."
  type    = "A"
  ttl     = 200
  data    = ["${yandex_alb_load_balancer.test-balancer.listener[0].endpoint[0].address[0].external_ipv4_address[0].address}"]
}

resource "yandex_dns_recordset" "rs2" {
  zone_id = "${yandex_dns_zone.zone1.id}"
  name    = "monitoring.dagobah88.store."
  type    = "A"
  ttl     = 200
  data    = ["${yandex_compute_instance.prometheus-monitoring-server.network_interface.0.nat_ip_address}"]
}

resource "yandex_dns_recordset" "rs3" {
  zone_id = "${yandex_dns_zone.zone1.id}"
  name    = "grafana.dagobah88.store."
  type    = "A"
  ttl     = 200
  data    = ["${yandex_compute_instance.prometheus-monitoring-server.network_interface.0.nat_ip_address}"]
}

output "web_loadbalancer_ip" {
  value = yandex_alb_load_balancer.test-balancer.listener[0].endpoint[0].address[0].external_ipv4_address[0].address
}

output "test_server_ip" {
  value = yandex_compute_instance.server-1.network_interface.0.nat_ip_address
}

output "runner_ip" {
  value = yandex_compute_instance.gitlab-runner-server.network_interface.0.nat_ip_address
}

output "monitoring_ip" {
  value = yandex_compute_instance.prometheus-monitoring-server.network_interface.0.nat_ip_address
}

output "zone1" {
  value = yandex_dns_zone.zone1.id
}
