terraform {
  required_providers {
    yandex = {
      source = "terraform-registry.storage.yandexcloud.net/yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = "AQAAAAAAavf7AATuweN3Juj4zUvwu6jIAF66Zbo"
  cloud_id  = "b1genblbl6n8ukns8o45"
  folder_id = "b1gdeeatjdo5ocpcm2uh"
  zone      = "ru-central1-a"
}


data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "dagobah88-bucket"
    region     = "ru-central1"
    key        = "terraform.tfstate"
    access_key = "YCAJEwoGZTNvKkrZGqBqMniHM"
    secret_key = "YCMgZzrC2Ag7EZQJaeVePCB7sNvI9KMGOA57zuZh"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}


data "yandex_compute_image" "my_image" {
  family = "ubuntu-1804-lts"
}


resource "yandex_compute_instance" "prod-server" {

  name        = "prod-server"
  platform_id = "standard-v1"

  resources {
    cores = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my_image.id}"
      size = 50
  }
  }
  network_interface {
#    subnet_id = "${yandex_vpc_subnet.subnet-a.id}"
    subnet_id = "e9bt8no9uj177oon79kh"
   nat       = true
  }

    metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }



}


resource "yandex_alb_target_group" "prod-foo" {
  name      = "prod-my-target-group"

  target {
#    subnet_id = "${yandex_vpc_subnet.subnet-a.id}"
    subnet_id = "e9bt8no9uj177oon79kh"
    ip_address   = "${yandex_compute_instance.prod-server.network_interface.0.ip_address}"
  }
}



resource "yandex_alb_virtual_host" "prod-virtual-host" {
  name      = "prod-virtual-host"
  http_router_id = yandex_alb_http_router.prod-my-router.id
  route {
    name = "prod-route"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.prod-backend-group.id
        timeout = "3s"
      }
    }
  }
}



resource "yandex_alb_backend_group" "prod-backend-group" {
  name      = "prod-my-backend-group"

  http_backend {
    name = "prod-backend-1"
    weight = 1
    port = 8080
    target_group_ids = ["${yandex_alb_target_group.prod-foo.id}"]
#    tls {
#      sni = "backend-domain.internal"
#    }
#    load_balancing_config {
#      panic_threshold = 50
#    }    
    healthcheck {
      timeout = "1s"
      interval = "3s"
      http_healthcheck {
        path  = "/"
      }
    }
#    http2 = "true"
  }
}


resource "yandex_alb_http_router" "prod-my-router" {
  name      = "prod-my-http-router"
}



resource "yandex_alb_load_balancer" "prod-balancer" {
  name        = "prod-load-balancer"

#  network_id  = "${yandex_vpc_network.my-net.id}"
  network_id = "enptveg5tus0sr6f6bq1"

  allocation_policy {
    location {
      zone_id   = "ru-central1-a"
#      subnet_id = "${yandex_vpc_subnet.subnet-a.id}" 
      subnet_id = "e9bt8no9uj177oon79kh"
    }
  }

  listener {
    name = "prod-my-listener"
    endpoint {
      address {
        external_ipv4_address {
        }
      }
      ports = [ 80 ]
    }    
    http {
      handler {
        http_router_id = "${yandex_alb_http_router.prod-my-router.id}"
      }
    }
    }
     
}

resource "yandex_dns_recordset" "prod-rs1" {
#  zone_id = "${yandex_dns_zone.zone1.id}"
  zone_id = data.terraform_remote_state.vpc.outputs.zone1
  name    = "dagobah88.store."
  type    = "A"
  ttl     = 200
  data    = ["${yandex_alb_load_balancer.prod-balancer.listener[0].endpoint[0].address[0].external_ipv4_address[0].address}"]
}

output "prod_web_loadbalancer_ip" {
  value = yandex_alb_load_balancer.prod-balancer.listener[0].endpoint[0].address[0].external_ipv4_address[0].address
}

output "prod_server_ip" {
  value = yandex_compute_instance.prod-server.network_interface.0.nat_ip_address
}
