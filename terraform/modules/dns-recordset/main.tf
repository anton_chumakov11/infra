terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}


resource "yandex_dns_recordset" "rs1" {
  zone_id = var.zone_id
  name    = var.name
  type    = "A"
  ttl     = 200
  data    = ["${var.data}"]
}
