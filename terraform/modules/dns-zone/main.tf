terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}




resource "yandex_dns_zone" "zone" {
  name        = var.zone_name
  description = var.zone_description

  labels = {
    label1 = var.lable
  }

  zone    = var.zone
  public  = true
}

data "yandex_dns_zone" "zone_id" {
  dns_zone_id = yandex_dns_zone.zone.id
}
