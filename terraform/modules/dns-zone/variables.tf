variable "zone_name" {
  default = "my-public-zone"
}

variable "zone_description" {
  default = "dagobah88 public zone"
}

variable "lable" {
  default = "dagobah88-public"
} 

variable "zone" {
  default = "dagobah88.store."
}  
