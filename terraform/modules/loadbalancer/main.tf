terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"


}

resource "yandex_alb_http_router" "my-router" {
  name      = "http-router-${var.env}"
}


resource "yandex_alb_load_balancer" "test-balancer" {
  name        = "dagobah-load-balancer-${var.env}"


  network_id = "enptveg5tus0sr6f6bq1"

  allocation_policy {
    location {
      zone_id   = "ru-central1-a"
      subnet_id = "e9bt8no9uj177oon79kh"
    }
  }

  listener {
    name = "my-listener-${var.env}"
    endpoint {
      address {
        external_ipv4_address {
        }
      }
      ports = [ 80 ]
    }
    http {
      handler {
        http_router_id = "${yandex_alb_http_router.my-router.id}"
      }
    }
    }

}



resource "yandex_alb_backend_group" "backend-group" {
  name      = "my-backend-group-${var.env}"

  http_backend {
    name = "backend-1-${var.env}"
    weight = 1
    port = 8080
    target_group_ids = ["${yandex_alb_target_group.foo.id}"]
    healthcheck {
      timeout = "1s"
      interval = "3s"
      http_healthcheck {
        path  = "/"
      }
    }
  }
}


resource "yandex_alb_virtual_host" "virtual-host" {
  name      = "virtual-host-${var.env}"
  http_router_id = yandex_alb_http_router.my-router.id
  route {
    name = "route-${var.env}"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.backend-group.id
        timeout = "3s"
      }
    }
  }
}

resource "yandex_alb_target_group" "foo" {
  name      = "my-target-group-${var.env}"

  target {
    subnet_id = "e9bt8no9uj177oon79kh"
    ip_address   = "${var.ip_address}"
  }
}

