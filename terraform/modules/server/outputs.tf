output "server_name" {
  value       = yandex_compute_instance.server.name
}

output "server_ip" {
  value = yandex_compute_instance.server.network_interface.0.nat_ip_address 
}

output "local_ip" {
  value = yandex_compute_instance.server.network_interface.0.ip_address
}
