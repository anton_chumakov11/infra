terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}



#provider "yandex" {
#  token     = "AQAAAAAAavf7AATuweN3Juj4zUvwu6jIAF66Zbo"
#  cloud_id  = "b1genblbl6n8ukns8o45"
#  folder_id = "b1gdeeatjdo5ocpcm2uh"
#  zone      = "ru-central1-a"
#}


data "yandex_compute_image" "my_image" {
  family = "ubuntu-1804-lts"
}


resource "yandex_compute_instance" "server" {

  name        = var.server_name
  platform_id = var.platform_id

  resources {
    cores = 2
    memory = 4
  }
    boot_disk {
      initialize_params {
        image_id = "${data.yandex_compute_image.my_image.id}"
        size = var.size
      }
    }
  network_interface {
    subnet_id = "e9bt8no9uj177oon79kh"
   nat       = true
  }

    metadata = {
#    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
      ssh-keys = "ubuntu:${file("${var.key}")}"
  }



}

