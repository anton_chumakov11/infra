variable "server_name" {
  default = "test"
}


variable "size" {
  default = 50
} 


variable "key" {
  default = "~/.ssh/id_rsa.pub"
} 

variable "platform_id" {
  default = "standard-v1"
}

