# 1. Choosing Monitoring System

Date: 2022-01-06

## Status

Accepted

## Context

We need to choose some monitoring system.

## Decision

We chose Prometheus because we have experience with it. It is also quite popular and has a large community. Plus, it's a free solution.

## Consequences
